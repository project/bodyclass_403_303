CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

In our daily basis as developers, we find sometimes some pages 
in our drupal projects that we need to layout, 
and by a coincidence of destiny, we don't find any different class 
in body to layout that page.
We have found a common problem in community with this issue. 
With this module we add a strong site-building 
solution to add classes to 403, 404 and a custom bunch of pages. 


 * For a full description of the module, visit the project page:
   https://drupal.org/project/bodyclass_403_404

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/bodyclass_403_404

REQUIREMENTS
------------

This module no require additional modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

You must configure the module in
/admin/config/development/bodyclass_403_404
You can find as well a link in configuration menu to config the module.


MAINTAINERS
-----------

Current maintainers:
 * Diego Guillermo (diegol_de_los_bosques) - https://www.drupal.org/u/diegol_de_los_bosques

This project has been sponsored by:
 * SDOS
 We create, develop and implement technology that feels.
